### VARS ###########################################
docker_container_nginx 		= lumen-webserver
docker_container_php 		= lumen-php-fpm
docker_container_mysql 		= lumen-mysql
docker_container_composer 	= lumen-composer
####################################################



up:
	@docker-compose up -d

stop:
	@docker-compose stop

build:
	@docker-compose build

restart:
	@make stop && make up



connect_nginx:
	@docker exec -it ${docker_container_nginx} bash

connect_php:
	@docker exec -it ${docker_container_php} bash

connect_mysql:
	@docker exec -it ${docker_container_mysql} bash

connect_composer:
	@docker exec -it ${docker_container_composer} bash




# 	LARAVEL
routeList:
	@docker exec -it ${docker_container_php} php artisan route:list




tarsBuild:
	@cd stuff/html-design/olp; tars build; cd ../../../;


