CREATE DATABASE IF NOT EXISTS lumen_db;
USE lumen_db;

CREATE USER IF NOT EXISTS 'test_user'@'localhost' IDENTIFIED BY 'test_pass';

GRANT ALL PRIVILEGES ON lumen_db.* TO 'test_user'@'localhost';
