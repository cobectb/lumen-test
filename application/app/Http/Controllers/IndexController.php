<?php

namespace App\Http\Controllers;

use App\Models\DataGrabbers\TreasuryDataGrabber;
use App\Models\DataGrabbingStatus;
use App\Models\Entry;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function update(Request $request)
    {
        ini_set('memory_limit', '1200M');

        $ret = [];
        $requestStatusCode = '200';
        $info = '';

        $grabbingStatus = DataGrabbingStatus::first();
        if(!$grabbingStatus){
            $grabbingStatus = new DataGrabbingStatus();
        }
        $grabbingStatus->status = 1;
        $grabbingStatus->save();

        #   тянем данные
        try {
            $dataArr = TreasuryDataGrabber::getData();

            #   берём только людей
            $dataArr = array_filter($dataArr, function($item){
                return $item['sdnType'] == 'Individual';
            });

//            $dataArr = array_slice($dataArr, 0, 20);

            #   сохраняем / обновляем
            foreach ($dataArr as $val) {
                $item = Entry::where('uid', $val['uid'])->first();
                if(!$item) {
                    $item = new Entry();
                }
                $item->setData($val);
                $item->save();
            }
        } catch (\Exception $e) {
            $requestStatusCode = 503;
            $info = 'service unavailable"';
        }


        $grabbingStatus->status = 0;
        $grabbingStatus->save();

        $ret = [
            'result'=>$requestStatusCode==200 ? true : false,
            'info' => $info,
            'code' => $requestStatusCode,
        ];


        return response()->json($ret);
    }



    public function state(Request $request)
    {
        $ret = [];

        $status = 'empty';
        $result = false;
        $grabbingStatus = DataGrabbingStatus::first();
        if($grabbingStatus){
            if($grabbingStatus->status == 1){
                $status = 'updating';
            } elseif(Entry::select()->count()){
                $status = 'ok';
                $result = true;
            }
        }

        $ret = [
            'result' =>$result,
            'info' =>$status,
        ];


        return response()->json($ret);
    }


    public function getNames(Request $request)
    {
        $ret = [];

        $namesArr = [];
        if($request->get('name')){
            $namesArr = array_filter(explode(' ', $request->get('name')));

            $query = Entry::select();
            foreach ($namesArr as $name){
                $query->orWhere('firstName', $name)
                      ->orWhere('lastName', $name);
            }
            $result = $query->get();
            if(count($result)) {
                foreach ($result as $entry){
                    $ret[] = $entry->json();
                }
            }

        }

        return response()->json($ret);
    }

}
