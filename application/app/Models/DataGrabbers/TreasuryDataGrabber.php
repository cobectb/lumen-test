<?php

namespace App\Models\DataGrabbers;


use Illuminate\Support\Facades\Http;

class TreasuryDataGrabber
{

    /**
     * Метод возвращает массив данных
     *
     * @returns array
    */
    public static function getData()
    {
        $ret = [];

        $resp = Http::get('https://www.treasury.gov/ofac/downloads/sdn.xml');
        $xmlString = $resp->body();
        $xml = simplexml_load_string($xmlString);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $ret = $array['sdnEntry'];

        return $ret;
    }


}
