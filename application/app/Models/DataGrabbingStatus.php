<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataGrabbingStatus extends Model
{
    protected $table = 'data_grabbing_status';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];





}
