<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{

    protected $table = 'entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function setData($data)
    {
        $this->uid = trim($data['uid']);
        $this->firstName = trim($data['firstName']);
        $this->lastName = trim($data['lastName']);
        $this->data = json_encode($data);
    }

    public function json(){
        return [
            'uid'=>$this->uid,
            'first_name'=>$this->firstName,
            'last_name'=>$this->lastName,
        ];
    }

}
