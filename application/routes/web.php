<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    echo "!";
    return $router->app->version();
});


$router->get('/update',  '\App\Http\Controllers\IndexController@update');
$router->get('/state',  '\App\Http\Controllers\IndexController@state');
$router->get('/get_names',  '\App\Http\Controllers\IndexController@getNames');
